;; $Id: font-cygwin-w32.el,v 1.1 2014/06/07 03:30:50 skimura Exp $

(custom-set-faces
 '(default ((t (:inherit nil :stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width normal :foundry "outline" :family "\202l\202r \203S\203V\203b\203N")))))
