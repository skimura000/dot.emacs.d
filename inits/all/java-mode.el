(add-hook 'java-mode-hook
          '(lambda ()
             (define-key java-mode-map "\C-c\C-c" 'my-copy-word)
             (define-key java-mode-map "\C-ct" 'gtags-find-tag)
             (define-key java-mode-map "\C-cr" 'gtags-find-rtag)
             (define-key java-mode-map "\C-cs" 'gtags-find-symbol)
             (define-key java-mode-map "\C-cf" 'gtags-find-file)
             (define-key java-mode-map "\C-c," 'gtags-pop-stack)
             ))
