;; $Id: search-web.el,v 1.2 2014/02/19 15:17:07 skimura Exp $

;; http://d.hatena.ne.jp/tomoya/20090703/1246610432 で紹介されていたソースのコピペ & 改変

(defvar search-engines
  '(("s" . "http://reference.sitepoint.com/search?q=%s")
    ("g" . "http://www.google.com/search?q=%s")
    ("gj" . "http://www.google.com/search?hl=ja&q=%s")
    ("ge" . "http://www.google.com/search?hl=en&q=%s")
    ("m" . "http://maps.google.co.jp/maps?hl=ja&q=%s")
    ("y" . "http://search.yahoo.co.jp/search?p=%s")
    ("yt" . "http://www.youtube.com/results?search_type=&search_query=%s&aq=f")
    ("tw" . "http://search.twitter.com/search?q=%s")
    ("goo" . "http://dictionary.goo.ne.jp/srch/all/%s/m0u/")
    ("a" . "http://www.answers.com/topic/%s")
    ("ew" . "http://www.google.com/cse?cx=004774160799092323420%%3A6-ff2s0o6yi&q=%s&sa=Search")
    ("eow" . "http://eow.alc.co.jp/%s/UTF-8/")
    ("z" . "http://www.amazon.com/s/url=search-alias%%3Daps&field-keywords=%s")
    ("zj" . "http://www.amazon.co.jp/gp/search?index=blended&field-keywords=%s")
    ("y" . "http://search.yahoo.com/search?p=%s")
    ("yj" . "http://search.yahoo.co.jp/search?p=%s")
    ("we" . "http://www.wikipedia.org/search-redirect.php?search=%s&language=en")
    ("wj" . "http://www.wikipedia.org/search-redirect.php?search=%s&language=ja")
    ("eijiro" . "http://eow.alc.co.jp/search?q=%s")
    )
  "A list is search engines list. keys engines nick, and value is search engine query.
Search word %s. In formatting url-hexify. Use %% to put a single % into output.")

(defun search-web (engine word)
  (browse-url
   (format (cdr (assoc engine search-engines)) (url-hexify-string word))))

(defun search-web-at-point (engine)
  "search web search engine for word on cursor.
arg is search-engines keys."
  (interactive "sSearch engine: ")
  (search-web engine (substring-no-properties (thing-at-point 'word))))

(defun search-web-region (engine)
  (interactive "sSearch engine: ")
  (let ((beg (mark))
        (end (point)))
        (search-web engine (buffer-substring-no-properties beg end))))

(defun search-web-input (engine)
  (interactive "sSearch engine: \n")
  (search-web engine (read-from-minibuffer "Search word: ")))

(defun search-web-dwim (engine)
  (interactive "sSearch engine: ")
  (if (region-active-p)
      (search-web-region engine)
    (let (word-with-properties)
      (setq word-with-properties (thing-at-point 'word))
      (if word-with-properties
          (search-web engine (substring-no-properties word-with-properties))
        (search-web-input engine)))))

(define-prefix-command  'search-web-map)
(global-set-key "\M-i"  'search-web-map)
(global-set-key "\M-ie" (lambda () (interactive) (search-web-dwim "eijiro")))
(global-set-key "\C-c\C-g" (lambda () (interactive) (search-web-input "g")))
