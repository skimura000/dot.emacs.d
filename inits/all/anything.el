;; $Id: anything.el,v 1.5 2015/05/19 09:37:33 skimura Exp $

(setq w3m-command "/home/skimura/bin/w3m")

(require 'anything-config)
;(require 'anything-gtags)
;(require 'anything-c-yasnippet-2)

(setq anything-sources
      '(
        anything-c-source-buffers
        anything-c-source-files-in-current-dir
        anything-c-source-recentf
        anything-c-source-extended-command-history
        anything-c-source-emacs-commands
        anything-c-source-google-suggest
        anything-c-source-imenu
;        anything-c-yasnippet-2
;        anything-c-source-gtags-select
        ))
(global-set-key (kbd "C-x ;") 'anything)
(global-set-key (kbd "C-x b") 'anything-for-files)
(global-set-key (kbd "M-y")   'anything-show-kill-ring)
(global-set-key (kbd "M-x")   'anything-M-x)


(autoload 'anything-grep-by-name "anything-grep" "anything-grep" t)
(autoload 'anything-grep "anything-grep" "anything-grep" t)
(setq anything-grep-alist
      '(("WorkLog"   ("egrep -Hin %s WorkLog.org" "~/Dropbox/"))
        ("TKCC Spec" ("egrep -Hin %s *.txt"       "~/work/tkoclcc/documents/spec/draft/class"))))
(global-set-key "\C-cg" 'anything-grep)
(global-set-key "\C-cG" 'anything-grep-by-name)
