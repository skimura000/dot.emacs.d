;; $Id: misc-settings.el,v 1.4 2015/05/19 09:38:06 skimura Exp $

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(transient-mark-mode 0)

(setq line-move-visual nil)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(setq split-width-threshold 1000)       ;; 左右分割させないように閾値を大きく設定

(setq recentf-max-menu-items 30)        ;; 表示するファイルの数
(setq recentf-max-saved-items 100)      ;; 保存するファイルの数
(setq kill-ring-max 100)                ;; kill-ring で保存される最大値

(add-hook 'before-save-hook
          '(lambda ()
             (cond ((equal mode-name "Shell-script") (untabify 1 (point-max)))
                   ((equal mode-name "Markdown")     (untabify 1 (point-max)))
                   ((equal mode-name "Emacs-Lisp")   (untabify 1 (point-max)))
                   ((equal mode-name "C/l")          (untabify 1 (point-max)))
                   ((equal mode-name "JavaScript")   (untabify 1 (point-max)))
                   ((equal mode-name "Text")         (untabify 1 (point-max)))
                   )))

;; 全てのバッファを閉じる
(defun close-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))

;; "Symbolic link to SVN-controlled source file; follow link? (yes or
;; no)" と尋ねるのを止めさせる。
(setq vc-follow-symlinks t)

(setq tramp-default-method "scpx")
;(setq tramp-shell-prompt-pattern "^\[:[a-zA-Z0-9_]+ skimura\]\$ ")
(setq tramp-remote-process-environment "LC_ALL=ja_JP.utf8")

(setq transient-mark-mode nil)

;; ビープ音を黙らせる
(setq ring-bell-function 'ignore)
