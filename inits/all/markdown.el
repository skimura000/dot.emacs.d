;; $Id: markdown.el,v 1.3 2015/04/28 04:02:27 skimura Exp $

(add-hook 'markdown-mode-hook
          '(lambda () 
             (define-key markdown-mode-map "\C-c\C-c" 'my-copy-word)
             (define-key markdown-mode-map "\C-h" 'delete-backward-char)
             (define-key markdown-mode-map (kbd "<backspace>") 'delete-backward-char)
             (define-key markdown-mode-map (kbd "<backspace>") 'delete-backward-char)

             ;; 文中に C ソース断片を書くとき、'*' や '->' が単語の一部
             ;; とみなされると auto-complete の使い勝手が悪いので、構文
             ;; テーブルを変更
             (auto-complete-mode 1)
             (modify-syntax-entry ?* ".")
             (modify-syntax-entry ?- ".")
             (modify-syntax-entry ?> ".")
             ))

;(add-to-list 'auto-mode-alist '("\\.txt$" . markdown-mode))
