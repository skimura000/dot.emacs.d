(add-hook 'nxml-mode-hook
          '(lambda ()
             (setq indent-tabs-mode nil)
             (setq nxml-slash-auto-complete-flag t)
             (define-key nxml-mode-map (kbd "TAB") 'completion-at-point)))
