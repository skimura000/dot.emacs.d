;; $Id: gdb.el,v 1.2 2015/07/09 15:46:22 skimura Exp $

;; https://gist.github.com/chokkan/5693497 のパクり

(setq gdb-many-windows t)
;(setq gdb-use-separate-io-buffer t) ;; 何の効果もないように思う。
(add-hook
 'gdb-mode-hook
 '(lambda ()
    (gud-tooltip-mode t)
    (gud-def gud-break-main "break main" nil "Set breakpoint at main.")
    (gud-set-bindings)
 ))
(setq gud-tooltip-echo-area nil)
 
(defun gud-set-bindings ()
    (progn
      (define-key gud-minor-mode-map (kbd "<S-f1>")  'gud-run)
      (define-key gud-minor-mode-map (kbd "<f1>")    'gud-cont)
      (define-key gud-minor-mode-map (kbd "<f2>")    'gud-next)
      (define-key gud-minor-mode-map (kbd "<S-f2>")  'gud-step)
      (define-key gud-minor-mode-map (kbd "<f3>")    'gud-up)
      (define-key gud-minor-mode-map (kbd "<S-f3>")  'gud-down)
      (define-key gud-minor-mode-map (kbd "<f4>")    'gud-finish)
      (define-key gud-minor-mode-map (kbd "<f5>")    'gud-break)
      (define-key gud-minor-mode-map (kbd "<S-f5>")  'gud-tbreak)
      (define-key gud-minor-mode-map (kbd "<f12>")   'gdb-many-windows)
      (define-key gud-minor-mode-map (kbd "<S-f12>") 'gdb-restore-windows)))
 
(defun gdb-set-clear-breakpoint ()
  (interactive)
  (if (or (buffer-file-name) (eq major-mode 'gdb-assembler-mode))
      (if (or
           (let ((start (- (line-beginning-position) 1))
                 (end (+ (line-end-position) 1)))
             (catch 'breakpoint
               (dolist (overlay (overlays-in start end))
                 (if (overlay-get overlay 'put-break)
                     (throw 'breakpoint t)))))
           (eq (car (fringe-bitmaps-at-pos)) 'breakpoint))
          (gud-remove nil)
        (gud-break nil))))
 
(defun gud-kill ()
  "Kill gdb process."
  (interactive)
  (with-current-buffer gud-comint-buffer (comint-skip-input))
  (kill-process (get-buffer-process gud-comint-buffer)))
