;; $Id: mylib.el,v 1.1 2014/02/05 03:03:15 skimura Exp $

(defun my-copy-word () 
  (interactive) 
  (let (b) 
       (progn 
         (save-excursion 
           (skip-chars-backward "a-zA-Z0-9_") 
           (setq b (point)) 
           (skip-chars-forward "a-zA-Z0-9_") 
           (kill-ring-save b (point))) 
         (skip-chars-forward "a-zA-Z0-9_")))) 
