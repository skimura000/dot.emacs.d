;; $Id: w3m.el,v 1.3 2015/05/19 09:38:35 skimura Exp $

;(autoload 'w3m-load "w3m" "w3m" t)
(setq w3m-command "/home/skimura/bin/w3m")
(setq browse-url-browser-function 'w3m-browse-url)
(setq w3m-user-agent "Mozilla/4.0")
(autoload 'w3m-browse-url "w3m" "Ask a WWW browser to show a URL." t)
(global-set-key "\C-xm" 'browse-url-at-point)
