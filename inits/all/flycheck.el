(defun load-file-with-cd (fname)
  (let ((save-cwd (expand-file-name "."))
        (full-fname (expand-file-name fname)))
    (cd (directory-file-name (file-name-directory full-fname)))
    (load-file full-fname)
    (cd save-cwd)))

(defvar fname-flycheck-include-path-setting ".flycheck-include-path")

(defun search-flycheck-include-path-setting0 (dname)
  (let ((fname-setting (concat dname "/" fname-flycheck-include-path-setting)))
    (if (file-exists-p fname-setting)
        (load-file-with-cd fname-setting)
      (let ((dname-parent (directory-file-name (file-name-directory dname))))
        (if (not (string= dname-parent dname))
            (search-flycheck-include-path-setting0 dname-parent))))))

(defun search-flycheck-include-path-setting (dname)
  (search-flycheck-include-path-setting0 (directory-file-name (expand-file-name dname))))

(add-hook 'flycheck-before-syntax-check-hook
          '(lambda ()
             (search-flycheck-include-path-setting ".")))
