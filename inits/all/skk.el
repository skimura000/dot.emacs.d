;; $Id: skk.el,v 1.2 2014/02/16 18:28:21 skimura Exp $

(global-set-key "\C-x\C-j" 'skk-mode)
(global-unset-key "\C-xj")
(global-unset-key "\C-xt")
(setq skk-large-jisyo "~/.dict/SKK-JISYO.L")
(setq skk-extra-jisyo-file-list
      (list "~/.dict/SKK-JISYO.jinmei"
            "~/.dict/SKK-JISYO.geo"
            "~/.dict/SKK-JISYO.station"
            "~/.dict/SKK-JISYO.propernoun"
            "~/.dict/SKK-JISYO.fullname"))
(setq skk-compare-jisyo-size-when-saving nil)
(setq skk-jisyo-save-count nil)

;; hh と mm については、skk-vars.el に記述のある元々の定義に含まれてい
;; たので残しておいた。
;; 自分で加えたのは @ で、デフォルトだと「トゥデイ」と変換されてイラつ
;; くので、そのまま @ が入力されるようにした。
(setq skk-rom-kana-rule-list '(("hh" "h" ("ッ" . "っ"))
                               ("mm" "m" ("ン" . "ん"))
                               ("@" nil "@")))
