;; $Id: misc-keybinds.el,v 1.1 2014/02/05 03:03:15 skimura Exp $

(define-key global-map "\C-h" 'delete-backward-char)
(define-key global-map (kbd "<backspace>") 'delete-backward-char)
(define-key global-map "\C-c\C-c" 'my-copy-word)
