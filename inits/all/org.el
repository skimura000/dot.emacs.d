;; $Id: org.el,v 1.12 2015/03/31 06:12:01 skimura Exp $

(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(setq org-startup-truncated nil)
(setq org-return-follows-link t)
(setq org-reverse-note-order t)
(setq org-directory "~/Documents/Dropbox/Documents/org/")
(setq org-default-notes-file (concat org-directory "WorkLog.org"))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
(setq my-org-private-notes-file (concat org-directory "PrivateLog.org"))
(setq org-capture-templates
      '(
        ("t" "ToDo(W)"          entry (file+headline org-default-notes-file     "ToDo")         "** TODO %?\n%U\nDEADLINE: %T\n%i"      :prepend t)
        ("s" "Schedule(W)"      entry (file+headline org-default-notes-file     "Schedule")     "** %?\n%T\n%U\n%i"                     :prepend t)
        ("e" "Event(W)"         entry (file+headline org-default-notes-file     "Event")        "** %U %?\n%i"                          :prepend t)
        ("m" "Memo(W)"          entry (file+headline org-default-notes-file     "Memo")         "** %U %?\n%i"                          :prepend t)
        ("i" "Item(W)"          entry (file+headline org-default-notes-file     "Item")         "** %U %?\n%i"                          :prepend t)
        ("T" "ToDo(P)"          entry (file+headline my-org-private-notes-file  "ToDo")         "** TODO %?\n%U\nDEADLINE: %T\n%i"      :prepend t)
        ("S" "Schedule(P)"      entry (file+headline my-org-private-notes-file  "Schedule")     "** %? %T\n%U\n%i"                      :prepend t)
        ("E" "Event(P)"         entry (file+headline my-org-private-notes-file  "Event")        "** %U %?\n%i"                          :prepend t)
        ("M" "Memo(P)"          entry (file+headline my-org-private-notes-file  "Memo")         "** %U %?\n%i"                          :prepend t)
        ("I" "Item(P)"          entry (file+headline my-org-private-notes-file  "Item")         "** %U %?\n%i"                          :prepend t)
        ))
(setq org-agenda-files (list org-default-notes-file
                             my-org-private-notes-file
                             (concat org-directory "Money.org")))
(setq org-todo-keywords
      '((sequence "TODO(t)" "ENGAGED(e)" "WAIT(w)" "|" "DONE(d)" "SOMEDAY(s)")))

(setq org-mobile-inbox-for-pull org-default-notes-file)
(setq org-mobile-directory "~/Documents/Dropbox/アプリ/MobileOrg")

(defun my-org-insert-custom-id ()
  (interactive)
  (insert "  :PROPERTIES:\n  :CUSTOM_ID: ")
  (insert (format-time-string "%Y%m%d%H%M%S" (current-time)))
  (insert "\n  :END:\n"))
(global-set-key "\C-ci" 'my-org-insert-custom-id)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-code ((t (:foreground "darkgreen")))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files (quote ("~/Documents/Dropbox/Documents/org/WorkLog.org" "~/Documents/Dropbox/Documents/org/PrivateLog.org" "~/Documents/Dropbox/Documents/org/Compiler.org"))))

(setq org-plantuml-jar-path "/home/skimura/apps/lib/plantuml.jar")
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (shell . t)
   (ruby . t)
   (plantuml . t)))
   
