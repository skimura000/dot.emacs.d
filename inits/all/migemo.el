;; $Id: migemo.el,v 1.1 2014/02/23 11:56:38 skimura Exp $

(when (and (executable-find "cmigemo")
           (require 'migemo nil t))
  (setq migemo-options '("-q" "--emacs"))
  (setq migemo-command "cmigemo")
  (setq migemo-dictionary "/home/skimura/apps/share/migemo/utf-8/migemo-dict")
  (setq migemo-user-dictionary nil)
  (setq migemo-regex-dictionary nil)
  (setq migemo-coding-system 'utf-8-unix)
  (load-library "migemo")
  (migemo-init)
)
