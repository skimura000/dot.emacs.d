;; $Id: auto-complete.el,v 1.3 2015/04/28 04:02:55 skimura Exp $

(require 'auto-complete)
(require 'auto-complete-config)
(define-key ac-complete-mode-map "\C-n" 'ac-next)
(define-key ac-complete-mode-map "\C-p" 'ac-previous)

(add-hook 'c-mode-hook
          '(lambda ()
             (auto-complete-mode)
             (setq ac-sources '(ac-source-gtags
                                ac-source-words-in-same-mode-buffers
                                ac-source-yasnippet
                                ))))

(add-hook 'c++-mode-hook
          '(lambda ()
             (auto-complete-mode)
             (setq ac-sources '(ac-source-gtags
                                ac-source-words-in-same-mode-buffers
                                ))))

(add-hook 'ruby-mode-hook
          '(lambda ()
             (auto-complete-mode)
             (setq ac-sources '(ac-source-abbrev
                                ac-source
                                ac-source-words-in-same-mode-buffers
                                ))))

(add-hook 'emacs-lisp-mode-hook
          '(lambda ()
             (auto-complete-mode)
             (setq ac-sources '(ac-source-abbrev
                                ac-source-features
                                ac-source-functions
                                ac-source-variables
                                ac-source-words-in-same-mode-buffers
                                ))))

(add-hook 'ruby-mode-hook
          '(lambda ()
             (auto-complete-mode)
             (setq ac-source '(ac-source
                               ac-source-words-in-same-mode-buffers
                               ))))

(add-hook 'mew-draft-mode-hook
          '(lambda ()
             (auto-complete-mode)
             (setq ac-source '(ac-source-yasnippet
                               ))))
