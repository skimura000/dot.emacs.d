;; $Id: font.el,v 1.3 2014/12/18 15:54:45 skimura Exp $

(defun setup-font ()
  (interactive)
  (when window-system
    (when (eq system-type 'windows-nt)
      (set-default-font "-*-Ricty Discord-normal-normal-normal-*-14-*-*-*-c-*-iso10646-1")
      )
    (when (eq system-type 'gnu/linux)
      (set-default-font "-*-Ricty Discord-normal-normal-normal-*-14-*-*-*-c-*-iso10646-1")
      )
    ))
(setup-font)

(defun setup-frame-font (frame)
  (setup-font))

(add-hook 'after-make-frame-functions 'setup-frame-font)

(provide 'conf-hook)
