;; $Id: font-home.el,v 1.1 2014/02/16 18:28:15 skimura Exp $

(defun setup-font ()
  (interactive)
  (when window-system
    (progn
      (create-fontset-from-fontset-spec
       "-*-*-*-*-*-*-*-*-*-*-*-*-fontset-mplus,
      ascii:-mplus-fxd-medium-r-semicondensed--12-120-75-75-c-60-iso8859-1,
      japanese-jisx0208:-mplus-gothic-medium-r-normal--12-120-75-75-c-120-jisx0208.1990-0,
      katakana-jisx0201:-mplus-gothic-medium-r-normal--12-120-75-75-c-60-jisx0201.1976-0")
      (set-default-font "fontset-mplus")
      (add-to-list 'default-frame-alist
                   '(font . "fontset-mplus")
                   '(menu-bar-lines . -1))
      )))
