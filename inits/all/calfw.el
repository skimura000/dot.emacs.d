;; $Id: calfw.el,v 1.3 2014/02/18 02:31:49 skimura Exp $

(require 'calfw)

(setq calendar-month-name-array
      ["Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"])
(setq calendar-day-name-array
      ["Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat"])
(setq calendar-week-start-day 0)

(require 'japanese-holidays)
(setq calendar-holidays
      (append japanese-holidays local-holidays other-holidays))

(autoload 'cfw:open-org-calendar "calfw-org" "calfw-org" t)
