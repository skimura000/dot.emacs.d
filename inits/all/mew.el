;; $Id: mew.el,v 1.2 2014/02/28 05:23:08 skimura Exp $

;;
;; Mew の設定
;;
(autoload 'mew "mew" nil t)

(add-hook 'mew-draft-mode-hook
          (lambda ()
            (ispell-change-dictionary "english")))

(load-library "org-mew")
