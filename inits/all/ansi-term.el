;; $Id: ansi-term.el,v 1.3 2014/04/09 15:42:01 skimura Exp $

(defadvice ansi-term (after ansi-term-after-advice (arg))
  "run hook as after advice"
  (run-hooks 'ansi-term-after-hook))
(ad-activate 'ansi-term)

;; バッファを削除するときに "Buffer "XXXX" has a running process; kill
;; it? (yes or no)" とか聞かれないようにする。
(add-hook 'ansi-term-after-hook
          '(lambda ()
             (process-kill-without-query (get-buffer-process (current-buffer)))))
