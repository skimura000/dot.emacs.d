;; $Id: shell-pop.el,v 1.3 2015/04/03 02:35:44 skimura Exp $

(autoload 'shell-pop "shell-pop" "shell-pop" t)
(setq shell-pop-term-shell "/home/skimura/bin/zsh")
(setq shell-pop-shell-type '("ansi-term" "*ansi-term*" (lambda () (ansi-term shell-pop-term-shell))))
(setq shell-pop-window-height 40)
(setq shell-pop-window-position "bottom")
(global-set-key [f8] 'shell-pop)
