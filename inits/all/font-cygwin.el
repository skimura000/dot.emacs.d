;; $Id: font-cygwin.el,v 1.1 2014/05/29 08:33:23 skimura Exp $

(defun setup-font ()
  (interactive)
  (when window-system
    (progn
      (create-fontset-from-fontset-spec
       "-*-*-*-*-*-*-*-*-*-*-*-*-fontset-mplus,
      ascii:-mplus-fxd-medium-*-*-*-12-*-*-*-*-*-iso8859-*,
      japanese-jisx0208:-mplus-gothic-medium-r-normal--12-*-*-*-*-*-jisx0208.1990-*,
      katakana-jisx0201:-mplus-gothic-medium-r-normal--12-*-*-*-*-*-jisx0201.1976-*")
      (set-default-font "fontset-mplus")
      (add-to-list 'default-frame-alist
                   '(font . "fontset-mplus")
                   '(menu-bar-lines . -1))
      )))
