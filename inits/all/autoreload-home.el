;; $Id: autoreload-home.el,v 1.2 2014/03/25 16:37:07 skimura Exp $

(setq my-autoreload-fileprop-alist
  '(("/home/skimura/Documents/Dropbox/Documents/org/WorkLog.org")
    ("/home/skimura/Documents/Dropbox/Documents/org/PrivateLog.org")))
(setq my-autoreload-timer-interval 3600)

(load-library "autoreload")
