(add-hook 'c-mode-hook 'c-turn-on-eldoc-mode)
(add-hook 'c++-mode-hook 'c-turn-on-eldoc-mode)

(defun c-eldoc-define-keybindings (map)
  (define-key map (kbd "C-c d") 'c-eldoc-force-cache-update))

(add-hook 'c-mode-hook
          (lambda ()
            (c-eldoc-define-keybindings c-mode-map)))

(add-hook 'c++-mode-hook
          (lambda ()
            (c-eldoc-define-keybindings c++-mode-map)))
