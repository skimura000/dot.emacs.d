(require 'markdown-mode)
(require 'edit-indirect)

;; 以下は、NS の「CGI Studio OTL 準拠テスト」に用いる testspec ファイルのための変更。
(defun markdown-get-lang-mode-around (f &rest args)
  (progn
    (message "%s" (car args))
    (cond ((string-match "^ttx\(" (car args))   (setcar args "xml"))
          ((string-match "^action$" (car args)) (setcar args "ruby")))
    (apply f args)))
(advice-add 'markdown-get-lang-mode :around #'markdown-get-lang-mode-around)

(add-hook 'edit-indirect-after-creation-hook
          '(lambda ()
             (if (and (string-match "\\.testspec\\*$" (buffer-name (current-buffer)))
                      (eq major-mode 'nxml-mode))
                 (rng-set-schema-file-and-validate "/home/skimura/Desktop/ns_qa/CGI Studio OTL準拠テスト/ttx.rnc"))))

(add-to-list 'auto-mode-alist '("\\.testspec\\'" . markdown-mode))
