(autoload 'gtags-mode "gtags" "" t)

(add-hook 'gtags-select-mode-hook
          '(lambda ()
             (define-key gtags-select-mode-map (kbd "RET") 'gtags-select-tag)
             (define-key gtags-select-mode-map "\C-c," 'gtags-pop-stack)))

(setq gtags-global-command "/home/skimura/apps/bin/global")
