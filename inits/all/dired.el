;; $Id: dired.el,v 1.2 2014/02/23 11:53:59 skimura Exp $

(add-hook 'dired-mode-hook (function (lambda ()
                                       (progn
                                           (define-key dired-mode-map "o" 'dired-open-file)
                                           (setq dired-dwim-target t)
                                           ))))
(defun dired-open-file ()
  "In dired, open the file named on this line."
  (interactive)
  (let* ((file (dired-get-filename)))
    (message "Opening %s..." file)
    (call-process "mimeopen" nil 0 nil (url-encode-url file))
    (message "Opening %s done" file)
    ))
