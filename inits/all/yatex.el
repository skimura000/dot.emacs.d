(setq auto-mode-alist
      (cons (cons "\\.tex" 'yatex-mode) auto-mode-alist))
(autoload 'yatex-mode "yatex" "Yet Another LaTeX mode" t)

(setq tex-command "./make.sh")
(setq dvi2-command "rundll32 shell32,ShellExec_RunDLL SumatraPDF -reuse-instance")
(setq YaTeX-inhibit-prefix-letter t)

(add-hook 'skk-mode-hook
          (lambda ()
            (if (eq major-mode 'yatex-mode)
                (progn
                  (define-key skk-j-mode-map "\\" 'self-insert-command)
                  (define-key skk-j-mode-map "$" 'YaTeX-insert-dollar)
                  ))
            ))

(defface YaTeX-font-lock-formula-face
  '((((class static-color)) (:bold nil))
    (((type tty)) (:bold nil))
    (((class color) (background light)) (:foreground "dark green"))
    (t (:bold nil :underline nil)))
  "Font Lock mode face used to highlight formula."
  :group 'font-lock-faces)

(defface YaTeX-font-lock-math-sub-face
  '((((class static-color)) (:bold nil))
    (((type tty)) (:bold nil))
    (((class color) (background light)) (:foreground "dark green" :underline nil))
    (t (:bold nil :underline nil)))
  "Font Lock mode face used to highlight subscripts in formula."
  :group 'font-lock-faces)

(defface YaTeX-font-lock-math-sup-face
  '((((class static-color)) (:bold nil))
    (((type tty)) (:bold nil))
    (((class color) (background light)) (:underline nil :foreground "dark green"))
    (t (:bold nil :underline nil)))
  "Font Lock mode face used to highlight superscripts in formula."
  :group 'font-lock-faces)

(defface YaTeX-font-lock-crossref-face
  '((((class color) (background light)) (:foreground "saddle brown"))
    (t (:bold nil :underline nil)))
  "Font Lock mode face used to highlight cross references."
  :group 'font-lock-faces)
