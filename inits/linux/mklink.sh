ln -s ../all/anything.el       05-anything.el     
ln -s ../all/auto-complete.el  05-auto-complete.el
ln -s ../all/org.el            09-org.el          
ln -s ../all/ansi-term.el      10-ansi-term.el    
ln -s ../all/c-mode.el         10-c-mode.el       
ln -s ../all/flycheck.el       10-flycheck.el     
ln -s ../all/font.el           10-font.el         
ln -s ../all/gtags-mode.el     10-gtags-mode.el   
ln -s ../all/mew.el            10-mew.el          
ln -s ../all/migemo.el         10-migemo.el       
ln -s ../all/mylib.el          10-mylib.el        
ln -s ../all/search-web.el     10-search-web.el   
ln -s ../all/shell-pop.el      10-shell-pop.el    
ln -s ../all/skk.el            10-skk.el          
ln -s ../all/w3m.el            10-w3m.el          
ln -s ../all/misc-settings.el  90-misc-settings.el
ln -s ../all/misc-keybinds.el  95-misc-keybinds.el
