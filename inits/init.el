;; $Id: init.el,v 1.4 2014/06/12 03:09:13 skimura Exp $

(when load-file-name
  (setq user-emacs-directory (file-name-directory load-file-name)))

(defun add-load-path-recursively (path)
  (let ((default-directory path))
    (add-to-list 'load-path default-directory)
    (normal-top-level-add-subdirs-to-load-path)))

;;
;; If EL-Get has not been installed, install it.
;;
(setq base-directory-of-el-get (locate-user-emacs-file "el-get"))
(unless (file-exists-p base-directory-of-el-get)
  (make-directory base-directory-of-el-get))

(add-load-path-recursively base-directory-of-el-get)

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(el-get-bundle init-loader)
(el-get-bundle auto-complete)
(el-get-bundle anything)
(el-get-bundle git-gutter)
(el-get-bundle magit)
(el-get-bundle org-mode)
(el-get-bundle mew)
(el-get-bundle migemo)
(el-get-bundle shell-pop)
(el-get-bundle ddskk)
(el-get-bundle anything-c-yasnippet-2)
(el-get-bundle wgrep)
(el-get-bundle yasnippet)
(el-get-bundle yasnippet-snippets)
(el-get-bundle gtags)
(el-get-bundle flycheck)
(el-get-bundle emacs-w3m)
(el-get-bundle go-mode)
(el-get-bundle company)
(el-get-bundle google-translate)
(el-get-bundle popwin)
(el-get-bundle rtags)
(el-get-bundle markdown-mode)
(el-get-bundle edit-indirect)

;;
;; パッケージを使わずにインストールしたものは、"~/.emacs.d/site-lisp"
;; もしくはそのサブディレクトリ以下に配置する。
;;
(add-load-path-recursively (expand-file-name (concat user-emacs-directory "site-lisp")))
(add-load-path-recursively (expand-file-name (concat user-emacs-directory "mylib")))

;;
;; init-loader でロードすべき設定を "~/.emacs.d/inits" 以下のファイルに
;; 記述する。
;;
;; ファイル名は、"10-hoge.el" のように <2桁の数字>-<任意の名前>.el とす
;; る。数字の順序でロードされる。
;;
(require 'init-loader)
(init-loader-load 
 (cond ((featurep 'dos-w32)                             ;; windows (msys2)
        (locate-user-emacs-file "inits/windows"))
       ((string-match "cygwin" (emacs-version))         ;; cygwin
        (locate-user-emacs-file "inits/cygwin"))
       (t
        (locate-user-emacs-file "inits/linux"))))
