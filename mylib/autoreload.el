;; $Id: autoreload.el,v 1.2 2014/03/25 16:35:17 skimura Exp $

(defvar my-autoreload-fileprop-alist nil)
(defvar my-autoreload-timer-interval 3600)

(setq my-autoreload-timer nil)

(defun my-autoreload-execute ()
  (let ((fileprop-alist my-autoreload-fileprop-alist)
        (filename nil)
        (buffer nil)
        (timer-cancel-p t))
    (while (setq filename (car (car fileprop-alist)))
      (setq fileprop-alist (cdr fileprop-alist))
      (if (get-file-buffer filename)
          (progn
            (setq timer-cancel-p nil)
            (if (and (setq buffer (get-file-buffer filename))
                     (not (verify-visited-file-modtime buffer)))
                (with-current-buffer buffer (revert-buffer t t))))))
    (if timer-cancel-p
        (progn
          (cancel-timer my-autoreload-timer)
          (setq my-autoreload-timer nil)))))

(defun my-autoreload-update-timer ()
  (interactive)
  (let ((fileprop-alist my-autoreload-fileprop-alist)
        (filename       nil)
        (buffer         nil)
        (timer-cancel-p t))
    (while (setq filename (car (car fileprop-alist)))
      (setq fileprop-alist (cdr fileprop-alist))
      (if (get-file-buffer filename)
          (progn
            (setq timer-cancel-p nil)
            (if (not my-autoreload-timer)
                (progn
                  (setq my-autoreload-timer (run-at-time 1 my-autoreload-timer-interval 'my-autoreload-execute))
                  (setq fileprop-alist nil))))))
    (if (and timer-cancel-p my-autoreload-timer)
        (cancel-timer my-autoreload-timer))))

(add-hook 'find-file-hook 'my-autoreload-update-timer)
