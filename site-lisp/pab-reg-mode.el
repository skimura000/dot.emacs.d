(defun pab-reg-mode ()
  "Private Account Book Registration Mode"
  (interactive)
  (kill-all-local-variables)
  (setq mode-name "pab-reg-mode")
  (setq major-mode 'pab-reg-mode)
  (run-hooks 'pab-reg-mode-hook))
(provide 'pab-reg-mode)

(defun pab-reg ()
  "Private Account Book Registration"
  (interactive)
  (switch-to-buffer (get-buffer-create "pab-reg"))
  (pab-reg-mode))
